    (function() {

    var RegApp = angular.module("RegApp", ["Admin"]);      
  var initReg = function(ctrl) {      
        ctrl.name = "";
        ctrl.dob = "";
        ctrl.email="";
        ctrl.contact="";
        ctrl.gender="";
        ctrl.address="";
        ctrl.country="";
        ctrl.password="";
  }   
        var createRegObject = function(ctrl) {
		return ({
			        name: ctrl.name,
                    dob: ctrl.dob,
                    gender:ctrl.gender,
                    address: ctrl.address,
                    country: ctrl.country,
                    email: ctrl.email,
                    contact:ctrl.contact,
                    password: ctrl.password,

                    timestamp: (new Date()).toString()
		});
	}
  

     var RegCtrl = function($scope,AdminSvc ) {
		var regCtrl = this;

		initReg(regCtrl);

		regCtrl.placeReg = function() { 
            AdminSvc.createReg(createRegObject(regCtrl))
				.then(function(){
                    regCtrl.status = "Thank you. Your registration has been successful."
                });		
		}

        var today = new Date();
        var minAge = 18;
        $scope.minAge = new Date(today.getFullYear() - minAge, today.getMonth(), today.getDate());
	};      
        
      
    

    //Strict Dependency Injection
    RegApp.controller("RegCtrl", [ "$scope","AdminSvc",RegCtrl ]);

})();