//Load libraries
var path = require("path");
var express = require("express");

//Create an instance of the app
var app = express();


//master registration
var allReg = [];

//Define routes
//
var createReg = function(reg) {
	return ({
				name: reg.name,
            	dob: reg.dob,
                gender:reg.gender,
                address: reg.address,
                country: reg.country,
                email: reg.email,
                contact:reg.contact,
                password: reg.password,

                timestamp: (new Date()).toString()
	});
}


//receive & process registration
app.get("/register", function(req, resp) {

	allReg.push(createReg(req.query));

	console.log("All registration:\n %s", JSON.stringify(allReg));

	resp.status(201).end();

});


app.use("/libs", express.static(path.join(__dirname, "bower_components")));

app.use(express.static(path.join(__dirname, "public")));


//Setup the server
app.set("port", process.env.APP_PORT || 3000);

app.listen(app.get("port"), function() {
	console.log("Application started at %s on port %d"
			, new Date(), app.get("port"));
});
